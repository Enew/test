package testing.testing;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Page {
	protected WebElement element;
	protected WebDriver driver;
	
	public Page(WebDriver driver )
	{
		this.driver = driver;
	}
	
	public abstract void searchElement();

}
