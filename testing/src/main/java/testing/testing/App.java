package testing.testing;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class App 
{
	 public static void main( String[] args )
	    {
	         System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
	    	
	    	WebDriver driver =  new ChromeDriver();
	    	Page1 page1 = new Page1(driver);
	    	page1.navigate();
	    	page1.searchElement();
	    	
	    	Page2 page2 = new Page2(driver);
	    	page2.searchElement();
	    	
	    	Page3 page3 = new Page3(driver);
	    	page3.searchElement();
	    	page3.existElement("Python");
	    	page3.outputElements();
	    	
	    	driver.close();
	    } 
}
