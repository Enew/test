package testing.testing;

import static org.junit.Assert.assertTrue;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Page3 extends Page {

	public Page3(WebDriver driver) {
		super(driver);
		
	}

public void searchElement()
{
	element = driver.findElement(By.className("downloadBox"));
    element.click();
}

public void existElement(String s)
{
	 assertTrue(driver.findElement(By.tagName("table")).getText().contains(s)); 
}

public void outputElements()
{
	for(int i=1;i<=5;i++){
        for (int j=1;j<=2;j++) {
            String txt = driver.findElement(By.xpath("//*[@id=\"mainContent\"]/table[1]/tbody/tr["+i+"]/td["+j+"]")).getText();

            System.out.print(txt + " \t");

        }
            System.out.println();
        }
}
}

